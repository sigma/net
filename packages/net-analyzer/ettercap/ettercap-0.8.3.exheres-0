# Copyright 2009 Arne Janbu <arnej@ampheus.de>
# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Ettercap release=v${PV} suffix=tar.gz ] \
    cmake

SUMMARY="A suite for man in the middle attacks and network mapping"
HOMEPAGE+=" https://www.${PN}-project.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config
    build+run:
        dev-libs/libbsd [[ note = [ automagic ] ]]
        dev-libs/libpcap
        dev-libs/pcre
        net-misc/curl[>=7.26.0]
        net-libs/libnet[>=1.1.6]
        sys-libs/ncurses
        sys-libs/zlib
        providers:openssl? ( dev-libs/openssl )
        providers:libressl? ( dev-libs/libressl:= )
        gtk? (
            dev-libs/atk
            dev-libs/glib:2
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:3[>=3.12.0]
            x11-libs/pango
        )
    test:
        dev-libs/check
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.8.3-cmake.patch
    "${FILES}"/${PNV}-avoid-global-EC_PTHREAD_NULL-variable.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUNDLED_CURL:BOOL=FALSE
    -DBUNDLED_LIBNET:BOOL=FALSE
    -DBUNDLED_LIBS:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DENABLE_CURSES:BOOL=TRUE
    -DENABLE_GEOIP:BOOL=FALSE
    -DENABLE_IPV6:BOOL=TRUE
    -DENABLE_LUA:BOOL=FALSE
    -DENABLE_PDF_DOCS:BOOL=FALSE
    -DENABLE_PLUGINS:BOOL=TRUE
    -DGTK_BUILD_TYPE:STRING=GTK3
    -DINSTALL_DESKTOP:BOOL=TRUE
    -DSYSTEM_CURL:BOOL=TRUE
    -DSYSTEM_LIBNET:BOOL=TRUE
    -DSYSTEM_LIBS:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    "gtk GTK"
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTS:BOOL=TRUE -DENABLE_TESTS:BOOL=FALSE'
)

