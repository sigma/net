# Copyright 2021 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require github [ user="cifsd-team" release=${PV} suffix=tar.gz ]
require systemd-service [ systemd_files=[ ksmbd.service ] ]

SUMMARY="cifsd kernel server userspace utilities"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    kerberos
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.40]
        net-libs/libnl:3.0[>=3.0]
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-shared
    --disable-static
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'kerberos krb5'
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( Documentation/configuration.txt )

src_install() {
    default

    insinto /etc/ksmbd
    newins smb.conf.example smb.conf
    edo chmod 0600 "${IMAGE}"/etc/ksmbd/smb.conf

    install_systemd_files
}

