# Copyright 2010 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Rewritten sieve plugin for dovecot (version 2.0+) deliver"
HOMEPAGE="http://wiki2.dovecot.org/Pigeonhole/Sieve"

# handle snapshots and releases separately
if [ -n "${MYREV}" ] ; then
    require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.11 ] ]

    DOWNLOADS="http://hg.rename-it.nl/dovecot-2.2-pigeonhole/archive/${MYREV}.tar.bz2"
    WORK="${WORKBASE}/dovecot-2-2-pigeonhole-${MYREV}"
else
    case "$(ever range 4)" in
        rc*|beta*|alpha*)
            MYPV="$(ever range 1-3).$(ever range 4)" ;;
        *) MYPV="${PV}"
    esac

    case "$(ever range 4)" in
        rc*) DOWNLOAD_PATH="rc" ;;
        beta*) DOWNLOAD_PATH="beta" ;;
        alpha*) DOWNLOAD_PATH="alpha" ;;
    esac

    DOWNLOADS="https://pigeonhole.dovecot.org/releases/2.3/${DOWNLOAD_PATH}/dovecot-2.3-pigeonhole-${MYPV}.tar.gz"
    WORK="${WORKBASE}/dovecot-2.3-pigeonhole-${MYPV}"
fi

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="( providers: libressl openssl ) [[ number-selected = exactly-one ]]"

DEPENDENCIES="
    build+run:
        net-mail/dovecot[>=2.3.11]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

# This probably should be made optional if someone cares for it. Besides that
# it currently duplicates some code from dovecot.
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-ubsan
    --with-docs
    --with-managesieve
    --without-fuzzer
    --without-ldap
)

DEFAULT_SRC_COMPILE_PARAMS=(
    CFLAGS="${CFLAGS}"
)

DEFAULT_SRC_TEST_PARAMS=(
    NOVALGRIND=1
)

